/**
  ******************************************************************************
  * @file    w25q256jw.h
  * @author  MCD Application Team (Modified by Arkan Ara)
  * @brief   This file contains all the description of the W25Q256JW QSPI memory.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __W25Q256JW_H
#define __W25Q256JW_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Components
  * @{
  */ 
  
/** @addtogroup w25q256jw
  * @{
  */

/** @defgroup W25Q256JW_Exported_Types
  * @{
  */
   
/**
  * @}
  */ 

/** @defgroup W25Q256JW_Exported_Constants
  * @{
  */
   
/** 
  * @brief  W25Q256JW Configuration  
  */  
#define W25Q256JW_FLASH_SIZE                  0x2000000 /* 256 MBits => 32MBytes */
#define W25Q256JW_BLOCK_SIZE           	      0x200  		/* 512 blocks of 64KBytes */
#define W25Q256JW_SECTOR_SIZE									0x2000  	/* 8192 sectors of 4kBytes */
#define W25Q256JW_PAGE_SIZE                   0x100     /* 131072 pages of 256 bytes */

#define W25Q256JW_DUMMY_CYCLES_READ           8
#define W25Q256JW_DUMMY_CYCLES_READ_QUAD_O		8
#define W25Q256JW_DUMMY_CYCLES_READ_QUAD_IO		4

#define W25Q256JW_CHIP_ERASE_MAX_TIME         410000
#define W25Q256JW_BLOCK_ERASE_MAX_TIME      	2000
#define W25Q256JW_SUBSECTOR_ERASE_MAX_TIME    400

/** 
  * @brief  W25Q256JW Commands  
  */  
/* Reset Operations */
#define RESET_ENABLE_CMD                     0x66
#define RESET_DEVICE_CMD                     0x99

/* Identification Operations */
#define READ_JEDEC_ID_CMD                    0x9F
#define READ_MANU_DEV_ID_CMD                 0x90
#define READ_UNIQUE_ID_CMD                	 0x4B



//#define READ_ID_CMD2                         0x9F
//#define MULTIPLE_IO_READ_ID_CMD              0xAF
#define READ_SERIAL_FLASH_DISCO_PARAM_CMD    0x5A

/* Read Operations */
#define READ_CMD                             0x03
#define FAST_READ_CMD                        0x0B
#define DUAL_OUT_FAST_READ_CMD               0x3B
#define DUAL_INOUT_FAST_READ_CMD             0xBB
#define QUAD_OUT_FAST_READ_CMD               0x6B
#define QUAD_OUT_FAST_READ_4BYTE_ADD_CMD     0x6C
#define QUAD_INOUT_FAST_READ_CMD             0xEB

/* Write Operations */
#define WRITE_ENABLE_CMD                     0x06
#define WRITE_DISABLE_CMD                    0x04

/* Register Operations */
#define READ_STATUS_REG1_CMD                 0x05
#define READ_STATUS_REG2_CMD                 0x35
#define READ_STATUS_REG3_CMD                 0x15
#define WRITE_STATUS_REG1_CMD                0x01
#define WRITE_STATUS_REG2_CMD                0x31
#define WRITE_STATUS_REG3_CMD                0x11


/* Program Operations */
#define PAGE_PROG_CMD                        0x02
//#define DUAL_IN_FAST_PROG_CMD                0xA2
//#define EXT_DUAL_IN_FAST_PROG_CMD            0xD2
#define QUAD_IN_FAST_PROG_CMD                0x32
#define QUAD_IN_FAST_PROG_4BYTE_ADD_CMD      0x34

//#define EXT_QUAD_IN_FAST_PROG_CMD            0x12

/* Erase Operations */
#define SECTOR_ERASE_CMD              		   0x20
#define BLOCK_ERASE_CMD                    	 0xD8
#define CHIP_ERASE_CMD                       0xC7

#define PROG_ERASE_RESUME_CMD                0x7A
#define PROG_ERASE_SUSPEND_CMD               0x75

/* One-Time Programmable Operations */
//#define READ_OTP_ARRAY_CMD                   0x4B
//#define PROG_OTP_ARRAY_CMD                   0x42


/* Security Registers Operations */
#define ERASE_SECURITY_REGISTER_CMD          0x44
#define PROG_SECURITY_REGISTER_CMD           0x42
#define READ_SECURITY_REGISTER_CMD         	 0x48

/* Security Registers Addresses */
#define SECURITY_REGISTER1_ADD         	 		 0x001000
#define SECURITY_REGISTER2_ADD         	 		 0x002000
#define SECURITY_REGISTER3_ADD         	 		 0x003000

#define W25Q256JW_SECURITY_REGISTER_SIZE     0x100     /* 256 bytes */



/** 
  * @brief  W25Q256JW Registers  
  */ 
/* Status Register 1 */
#define W25Q256JW_SR1_WEIP                   ((uint8_t)0x01)    /*!< Write/Erase in progress */
#define W25Q256JW_SR1_WREN                   ((uint8_t)0x02)    /*!< Write enable latch */



/* Status Register 2 */
#define W25Q256JW_SR2_QE                 		 ((uint8_t)0x02) 		/*!< Quad Enable (QE) � Volatile/Non-Volatile Writable */
#define W25Q256JW_SR2_ERPR_SUS           		 ((uint8_t)0x40)    /*!< Erase/Program Suspend Status (SUS) */

/* Status Register 3 */
#define W25Q256JW_SR3_ADS_4B            		 ((uint8_t)0x01) 		/*!< Current Address Mode (ADS) � Status Only */
#define W25Q256JW_SR3_ADP_4B           			 ((uint8_t)0x02)    /*!< Power-Up Address Mode (ADP) � Non-Volatile Writable */



#define FLASH_MEMORY_SECTOR_SIZE         				0x1000
#define FLASH_MEMORY_PAGE_SIZE									0x100
#define FLASH_MEMORY_PAGE_IN_SECTOR_NUM  				(FLASH_MEMORY_SECTOR_SIZE/FLASH_MEMORY_PAGE_SIZE)  /* 0x10 */	

/**
  * @}
  */
  
/** @defgroup W25Q256JW_Exported_Functions
  * @{
  */ 
/**
  * @}
  */ 
      
/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */

/* Prototypes */
void FlashMemory_Init(void);
void Save_ECG_callback(void);

HAL_StatusTypeDef CSP_QSPI_Erase_Sector(uint32_t SectorAddress);
HAL_StatusTypeDef CSP_QSPI_Write(uint8_t* buffer, uint32_t address, uint32_t buffer_size);
HAL_StatusTypeDef CSP_QSPI_Write_4Byte_Add(uint8_t* pData, uint32_t WriteAddr, uint32_t Size);
HAL_StatusTypeDef CSP_QSPI_Read(uint8_t* pData, uint32_t ReadAddr, uint32_t Size);
HAL_StatusTypeDef CSP_QSPI_Read_4Byte_Add(uint8_t* pData, uint32_t ReadAddr, uint32_t Size);
HAL_StatusTypeDef CSP_QSPI_Erase_Block(uint32_t EraseStartAddress, uint32_t EraseEndAddress);
HAL_StatusTypeDef CSP_QSPI_Erase_Chip (void);
HAL_StatusTypeDef CSP_QSPI_ReadStatus(uint8_t* pStatus, uint32_t ReadStatusNum);
HAL_StatusTypeDef CSP_QSPI_ReadManufacturerDeviceID(uint8_t* pManufacturerDeviceID);
HAL_StatusTypeDef CSP_QSPI_ReadUniqueID(uint8_t* pUniqueID);
HAL_StatusTypeDef CSP_QSPI_ReadJEDECID(uint8_t* pJEDECID);
HAL_StatusTypeDef CSP_QSPI_Erase_Security_Reg(uint8_t RegisterNum);
HAL_StatusTypeDef CSP_QSPI_Write_Security_Reg(uint8_t* pData, uint8_t RegisterNum, uint8_t WriteAddr, uint8_t Size);
HAL_StatusTypeDef CSP_QSPI_Read_Security_Reg(uint8_t* pData,  uint8_t RegisterNum, uint8_t ReadAddr, uint8_t Size);
HAL_StatusTypeDef CSP_QSPI_Write_Sector(uint8_t* pData, uint32_t Sector_num);


#ifdef __cplusplus
}
#endif

#endif /* __W25Q256JW_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
