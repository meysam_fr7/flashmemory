/**
  ******************************************************************************
  * @file    FlashMemory.c
  * @author  Meysam Noroozi
  * @brief   W25Q256JW
  ******************************************************************************
  */
	
/* Includes ------------------------------------------------------------------*/
#include "quadspi.h"
#include "w25q256jw.h"
#include "cmsis_os.h"


/* Private includes -----------------------------------------------------------*/



/* Private typedef -----------------------------------------------------------*/


/* Private defines -----------------------------------------------------------*/


/* Private macros ------------------------------------------------------------*/
extern int8_t compression_output[2*FLASH_MEMORY_SECTOR_SIZE];



/* Private variables ---------------------------------------------------------*/
osThreadId_t MyTaskSaveECGId;
const osThreadAttr_t MyTaskSaveECG_attr = {
    .name = CFG_MY_TASK_SAVE_ECG_NAME,
    .attr_bits = CFG_MY_TASK_SAVE_ECG_ATTR_BITS,
    .cb_mem = CFG_MY_TASK_SAVE_ECG_CB_MEM,
    .cb_size = CFG_MY_TASK_SAVE_ECG_CB_SIZE,
    .stack_mem = CFG_MY_TASK_SAVE_ECG_STACK_MEM,
    .priority = CFG_MY_TASK_SAVE_ECG_PRIORITY,
    .stack_size = CFG_MY_TASK_SAVE_ECG_STACK_SIZE
};

uint8_t SectorToWriteData[FLASH_MEMORY_SECTOR_SIZE];
uint32_t ECG_data_Sector_Num = 5;


/* Global variables ----------------------------------------------------------*/


/* Private functions prototypes-----------------------------------------------*/
static HAL_StatusTypeDef QSPI_WriteEnable(void);
static HAL_StatusTypeDef QSPI_WriteDisable(void);
static HAL_StatusTypeDef QSPI_AutoPollingMemReady(void);
static HAL_StatusTypeDef QSPI_ResetChip(void);
static HAL_StatusTypeDef QSPI_Configuration(void);
static void Save_ECG_data(void *argument);

/* Functions Definition ------------------------------------------------------*/

void FlashMemory_Init(void) {
	/** FreeRTOS system task creation to Save ECG on Flash Memory	(Save_ECG_data)*/
	MyTaskSaveECGId = osThreadNew(Save_ECG_data, NULL, &MyTaskSaveECG_attr);	
}
/* QUADSPI init function */
HAL_StatusTypeDef CSP_QUADSPI_Init(void)
{
    //prepare QSPI peripheral for ST-Link Utility operations
	hqspi.Instance = QUADSPI;
    if (HAL_QSPI_DeInit(&hqspi) != HAL_OK) {
        return HAL_ERROR;
    }

    MX_QUADSPI_Init();

    if (QSPI_ResetChip() != HAL_OK) {
        return HAL_ERROR;
    }

    HAL_Delay(1);

    if (QSPI_AutoPollingMemReady() != HAL_OK) {
        return HAL_ERROR;
    }

    if (QSPI_WriteEnable() != HAL_OK) {

        return HAL_ERROR;
    }

    if (QSPI_Configuration() != HAL_OK) {
        return HAL_ERROR;
    }

    return HAL_OK;
}

HAL_StatusTypeDef CSP_QSPI_EnableMemoryMappedMode(void)
{

    QSPI_CommandTypeDef sCommand;
    QSPI_MemoryMappedTypeDef sMemMappedCfg;

    /* Enable Memory-Mapped mode-------------------------------------------------- */

    sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    sCommand.AddressSize = QSPI_ADDRESS_24_BITS;
    sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
    sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
    sCommand.AddressMode = QSPI_ADDRESS_4_LINES;
    sCommand.DataMode = QSPI_DATA_4_LINES;
    sCommand.NbData = 0;
    sCommand.Address = 0;
    sCommand.Instruction = QUAD_INOUT_FAST_READ_CMD;
    sCommand.DummyCycles = W25Q256JW_DUMMY_CYCLES_READ_QUAD_O;

    sMemMappedCfg.TimeOutActivation = QSPI_TIMEOUT_COUNTER_DISABLE;

    if (HAL_QSPI_MemoryMapped(&hqspi, &sCommand, &sMemMappedCfg) != HAL_OK) {
        return HAL_ERROR;
    }
    return HAL_OK;
}

/**
  * @brief This function is used to erase all blocks.
	*					It uses Chip Erase command to do it.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_Erase_Chip(void)
{
    QSPI_CommandTypeDef sCommand;


    if (QSPI_WriteEnable() != HAL_OK) {
        return HAL_ERROR;
    }


    /* Erasing Sequence --------------------------------- */
    sCommand.Instruction = CHIP_ERASE_CMD;
    sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    sCommand.AddressSize = QSPI_ADDRESS_24_BITS;
    sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
    sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
    sCommand.AddressMode = QSPI_ADDRESS_NONE;
    sCommand.Address = 0;
    sCommand.DataMode = QSPI_DATA_NONE;
    sCommand.DummyCycles = 0;


    if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QSPI_TIMEOUT_DEFAULT_VALUE)
        != HAL_OK) {
        return HAL_ERROR;
    }

    if (QSPI_AutoPollingMemReady() != HAL_OK) {
        return HAL_ERROR;
    }

    return HAL_OK;
}

HAL_StatusTypeDef QSPI_AutoPollingMemReady(void)
{

    QSPI_CommandTypeDef sCommand;
    QSPI_AutoPollingTypeDef sConfig;

    /* Configure automatic polling mode to wait for memory ready ------ */
    sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    sCommand.Instruction = READ_STATUS_REG1_CMD;
    sCommand.AddressMode = QSPI_ADDRESS_NONE;
    sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    sCommand.DataMode = QSPI_DATA_1_LINE;
    sCommand.DummyCycles = 0;
    sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
    sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    sConfig.Match = 0x00;
    sConfig.Mask = W25Q256JW_SR1_WEIP;
    sConfig.MatchMode = QSPI_MATCH_MODE_AND;
    sConfig.StatusBytesSize = 1;
    sConfig.Interval = 0x10;
    sConfig.AutomaticStop = QSPI_AUTOMATIC_STOP_ENABLE;

    if (HAL_QSPI_AutoPolling(&hqspi, &sCommand, &sConfig, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
    {
        return HAL_ERROR;
    }

    return HAL_OK;
}

/**
  * @brief This function is used to enable write.
	*					It uses Write Enable command and sets the Write Enable Latch (WEL) bit in the Status Register to a 1.
	* 				It uses before Page Program, Quad Page Program, Sector Erase, Block Erase, Chip Erase, Write Status Register and Erase/Program Security Registers instruction
  * @retval HAL status
  */
static HAL_StatusTypeDef QSPI_WriteEnable(void)
{
    QSPI_CommandTypeDef sCommand;
    QSPI_AutoPollingTypeDef sConfig;

    /* Enable write operations ------------------------------------------ */
    sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    sCommand.Instruction = WRITE_ENABLE_CMD;
    sCommand.AddressMode = QSPI_ADDRESS_NONE;
    sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    sCommand.DataMode = QSPI_DATA_NONE;
    sCommand.DummyCycles = 0;
    sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
    sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QSPI_TIMEOUT_DEFAULT_VALUE)
        != HAL_OK) {
        return HAL_ERROR;
    }

    /* Configure automatic polling mode to wait for write enabling ---- */
    sConfig.Match = 0x02;
    sConfig.Mask = 0x02;
    sConfig.MatchMode = QSPI_MATCH_MODE_AND;
    sConfig.StatusBytesSize = 1;
    sConfig.Interval = 0x10;
    sConfig.AutomaticStop = QSPI_AUTOMATIC_STOP_ENABLE;

    sCommand.Instruction = READ_STATUS_REG1_CMD;
    sCommand.DataMode = QSPI_DATA_1_LINE;
    if (HAL_QSPI_AutoPolling(&hqspi, &sCommand, &sConfig,
                             HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return HAL_ERROR;
    }

    return HAL_OK;
}

/**
  * @brief This function is used to disable write.
	*					It uses Write Disable command and resets the Write Enable Latch (WEL) bit in the Status Register to a 0.
	* @note		The WEL bit is automatically reset after Power-up and upon completion of the Write Status Register, 
	* 				Erase/Program Security Registers, Page Program, Quad Page Program, Sector Erase, Block Erase, Chip Erase and Reset instructions.
  * @retval HAL status
  */
static HAL_StatusTypeDef QSPI_WriteDisable(void)
{
    QSPI_CommandTypeDef sCommand;
    QSPI_AutoPollingTypeDef sConfig;

    /* Enable write operations ------------------------------------------ */
    sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    sCommand.Instruction = WRITE_DISABLE_CMD;
    sCommand.AddressMode = QSPI_ADDRESS_NONE;
    sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    sCommand.DataMode = QSPI_DATA_NONE;
    sCommand.DummyCycles = 0;
    sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
    sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;

    if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QSPI_TIMEOUT_DEFAULT_VALUE)
        != HAL_OK) {
        return HAL_ERROR;
    }

    /* Configure automatic polling mode to wait for write enabling ---- */
    sConfig.Match = 0x02;
    sConfig.Mask = 0x02;
    sConfig.MatchMode = QSPI_MATCH_MODE_AND;
    sConfig.StatusBytesSize = 1;
    sConfig.Interval = 0x10;
    sConfig.AutomaticStop = QSPI_AUTOMATIC_STOP_ENABLE;

    sCommand.Instruction = READ_STATUS_REG1_CMD;
    sCommand.DataMode = QSPI_DATA_1_LINE;
    if (HAL_QSPI_AutoPolling(&hqspi, &sCommand, &sConfig,
                             HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
        return HAL_ERROR;
    }

    return HAL_OK;
}

/**
  * @brief This function is used to erase 64KB block.
	*					It uses 64KB Block Erase command to do it.
	* @param EraseStartAddress Start address to be erosen.
	* @param EraseEndAddress End address to be erosen.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_Erase_Block(uint32_t EraseStartAddress, uint32_t EraseEndAddress)
{

    QSPI_CommandTypeDef sCommand;

    EraseStartAddress = EraseStartAddress
                        - EraseStartAddress % W25Q256JW_BLOCK_SIZE;

    /* Erasing Sequence -------------------------------------------------- */
    sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    sCommand.AddressSize = QSPI_ADDRESS_24_BITS;
    sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
    sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
    sCommand.Instruction = BLOCK_ERASE_CMD;
    sCommand.AddressMode = QSPI_ADDRESS_1_LINE;

    sCommand.DataMode = QSPI_DATA_NONE;
    sCommand.DummyCycles = 0;

    while (EraseEndAddress >= EraseStartAddress)
    {
        sCommand.Address = (EraseStartAddress & 0x0FFFFFFF);

        if (QSPI_WriteEnable() != HAL_OK) {
            return HAL_ERROR;
        }

        if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QSPI_TIMEOUT_DEFAULT_VALUE)
            != HAL_OK) {
            return HAL_ERROR;
        }
        EraseStartAddress += W25Q256JW_BLOCK_SIZE;

        if (QSPI_AutoPollingMemReady() != HAL_OK) {
            return HAL_ERROR;
        }
    }

    return HAL_OK;
}

/**
  * @brief This function is used to reset chip.
	*					It uses Enable Reset and Reset Device commands to do it.
  * @retval HAL status
  */
HAL_StatusTypeDef QSPI_ResetChip()
{
    QSPI_CommandTypeDef sCommand;
    uint32_t temp = 0;
    /* Erasing Sequence -------------------------------------------------- */
    sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    sCommand.AddressSize = QSPI_ADDRESS_24_BITS;
    sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
    sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
    sCommand.Instruction = RESET_ENABLE_CMD;
    sCommand.AddressMode = QSPI_ADDRESS_NONE;
    sCommand.Address = 0;
    sCommand.DataMode = QSPI_DATA_NONE;
    sCommand.DummyCycles = 0;

    if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QSPI_TIMEOUT_DEFAULT_VALUE)
        != HAL_OK) {
        return HAL_ERROR;
    }
    for (temp = 0; temp < 0x2f; temp++) {
        __NOP();
    }

    sCommand.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    sCommand.AddressSize = QSPI_ADDRESS_24_BITS;
    sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    sCommand.DdrMode = QSPI_DDR_MODE_DISABLE;
    sCommand.SIOOMode = QSPI_SIOO_INST_EVERY_CMD;
    sCommand.Instruction = RESET_DEVICE_CMD;
    sCommand.AddressMode = QSPI_ADDRESS_NONE;
    sCommand.Address = 0;
    sCommand.DataMode = QSPI_DATA_NONE;
    sCommand.DummyCycles = 0;

    if (HAL_QSPI_Command(&hqspi, &sCommand, HAL_QSPI_TIMEOUT_DEFAULT_VALUE)
        != HAL_OK) {
        return HAL_ERROR;
    }
    return HAL_OK;
}

/**
  * @brief This function is used to erase sector.
	*					It uses Sector Erase command to do it.
	* @param SectorAddress Sector address to be erosen.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_Erase_Sector(uint32_t SectorAddress)
{
  QSPI_CommandTypeDef s_command;

  /* Initialize the erase command */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = SECTOR_ERASE_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_1_LINE;
  s_command.AddressSize       = QSPI_ADDRESS_24_BITS;
  s_command.Address           = SectorAddress;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_NONE;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Enable write operations */
  if (QSPI_WriteEnable() != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Send the command */
  if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Configure automatic polling mode to wait for end of erase */
  if (QSPI_AutoPollingMemReady() != HAL_OK)
  {
    return HAL_ERROR;
  }

  return HAL_OK;
}

/**
  * @brief This function is used to write data on QSPI 4 lines up to 256 Bytes.
	*					It uses Quad Input Page Program command to write data.
	* @param pData Pointer to write buffer data.
	* @param ReadAddr Address to be written.
	* @param Size Number of bytes to be written.
  * @note		To write an address that written before, MUST use erase functions first.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_Write(uint8_t* pData, uint32_t WriteAddr, uint32_t Size)
{
  QSPI_CommandTypeDef s_command;
  uint32_t end_addr, current_size, current_addr;

  /* Calculation of the size between the write address and the end of the page */
  current_size = W25Q256JW_PAGE_SIZE - (WriteAddr % W25Q256JW_PAGE_SIZE);

  /* Check if the size of the data is less than the remaining place in the page */
  if (current_size > Size)
  {
    current_size = Size;
  }

  /* Initialize the adress variables */
  current_addr = WriteAddr;
  end_addr = WriteAddr + Size;

  /* Initialize the program command */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = QUAD_IN_FAST_PROG_CMD; /* EXT_QUAD_IN_FAST_PROG_CMD */
  s_command.AddressMode       = QSPI_ADDRESS_1_LINE;
  s_command.AddressSize       = QSPI_ADDRESS_24_BITS;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_4_LINES;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Perform the write page by page */
  do
  {
    s_command.Address = current_addr;
    s_command.NbData  = current_size;

    /* Enable write operations */
    if (QSPI_WriteEnable() != HAL_OK)
    {
      return HAL_ERROR;
    }

    /* Configure the command */
    if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
    {
      return HAL_ERROR;
    }

    /* Transmission of the data */
    if (HAL_QSPI_Transmit(&hqspi, pData, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
    {
      return HAL_ERROR;
    }

    /* Configure automatic polling mode to wait for end of program */
    if (QSPI_AutoPollingMemReady() != HAL_OK)
    {
      return HAL_ERROR;
    }

    /* Update the address and size variables for next page programming */
    current_addr += current_size;
    pData += current_size;
    current_size = ((current_addr + W25Q256JW_PAGE_SIZE) > end_addr) ? (end_addr - current_addr) : W25Q256JW_PAGE_SIZE;
  } while (current_addr < end_addr);

  return HAL_OK;
}

/**
  * @brief This function is used to write data on QSPI 4 lines up to 256 Bytes with 32-bit address.
	*					It uses Quad Input Page Program with 4-Byte Address command to write data.
	* @param pData Pointer to write buffer data.
	* @param ReadAddr Address to be written.
	* @param Size Number of bytes to be written.
  * @note		To write an address that written before, MUST use erase functions first.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_Write_4Byte_Add(uint8_t* pData, uint32_t WriteAddr, uint32_t Size)
{
  QSPI_CommandTypeDef s_command;
  uint32_t end_addr, current_size, current_addr;

  /* Calculation of the size between the write address and the end of the page */
  current_size = W25Q256JW_PAGE_SIZE - (WriteAddr % W25Q256JW_PAGE_SIZE);

  /* Check if the size of the data is less than the remaining place in the page */
  if (current_size > Size)
  {
    current_size = Size;
  }

  /* Initialize the adress variables */
  current_addr = WriteAddr;
  end_addr = WriteAddr + Size;

  /* Initialize the program command */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = QUAD_IN_FAST_PROG_4BYTE_ADD_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_1_LINE;
  s_command.AddressSize       = QSPI_ADDRESS_32_BITS;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_4_LINES;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Perform the write page by page */
  do
  {
    s_command.Address = current_addr;
    s_command.NbData  = current_size;

    /* Enable write operations */
    if (QSPI_WriteEnable() != HAL_OK)
    {
      return HAL_ERROR;
    }

    /* Configure the command */
    if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
    {
      return HAL_ERROR;
    }

    /* Transmission of the data */
    if (HAL_QSPI_Transmit(&hqspi, pData, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
    {
      return HAL_ERROR;
    }

    /* Configure automatic polling mode to wait for end of program */
    if (QSPI_AutoPollingMemReady() != HAL_OK)
    {
      return HAL_ERROR;
    }

    /* Update the address and size variables for next page programming */
    current_addr += current_size;
    pData += current_size;
    current_size = ((current_addr + W25Q256JW_PAGE_SIZE) > end_addr) ? (end_addr - current_addr) : W25Q256JW_PAGE_SIZE;
  } while (current_addr < end_addr);

  return HAL_OK;
}

/**
  * @brief This function is used to read data on QSPI 4 lines.
	*					It uses Fast Read Quad Output command to read data.
	* @param pData Pointer to read buffer data.
	* @param ReadAddr Address to be read.
	* @param Size Number of bytes to be read.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_Read(uint8_t* pData, uint32_t ReadAddr, uint32_t Size)
{
  QSPI_CommandTypeDef s_command;

  /* Initialize the read command */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = QUAD_OUT_FAST_READ_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_1_LINE;
  s_command.AddressSize       = QSPI_ADDRESS_24_BITS;
  s_command.Address           = ReadAddr;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_4_LINES;
  s_command.DummyCycles       = W25Q256JW_DUMMY_CYCLES_READ_QUAD_O;
  s_command.NbData            = Size;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Configure the command */
  if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Set S# timing for Read command */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_3_CYCLE);

  /* Reception of the data */
  if (HAL_QSPI_Receive(&hqspi, pData, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Restore S# timing for nonRead commands */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_6_CYCLE);

  return HAL_OK;
}

/**
  * @brief This function is used to read data on QSPI 4 lines with 32-bit address.
	*					It uses Fast Read Quad Output with 4-Byte Address command to read data.
	* @param pData Pointer to read buffer data.
	* @param ReadAddr Address to be read.
	* @param Size Number of bytes to be read.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_Read_4Byte_Add(uint8_t* pData, uint32_t ReadAddr, uint32_t Size)
{
  QSPI_CommandTypeDef s_command;

  /* Initialize the read command */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = QUAD_OUT_FAST_READ_4BYTE_ADD_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_1_LINE;
  s_command.AddressSize       = QSPI_ADDRESS_32_BITS;
  s_command.Address           = ReadAddr;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_4_LINES;
  s_command.DummyCycles       = W25Q256JW_DUMMY_CYCLES_READ_QUAD_O;
  s_command.NbData            = Size;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Configure the command */
  if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Set S# timing for Read command */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_3_CYCLE);

  /* Reception of the data */
  if (HAL_QSPI_Receive(&hqspi, pData, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Restore S# timing for nonRead commands */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_6_CYCLE);

  return HAL_OK;
}

/**
  * @brief This function is used to read status registers.
	*					It uses Fast Read Quad Output command to read data.
	* @param pStatus Pointer to read status data.
	* @param ReadStatusNum Status register number.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_ReadStatus(uint8_t* pStatus, uint32_t ReadStatusNum)
{
  QSPI_CommandTypeDef s_command;
	/* Initialize the read status command */
	switch(ReadStatusNum){
		case 1 :
			s_command.Instruction 	= READ_STATUS_REG1_CMD;
			break;
		case 2 :
			s_command.Instruction 	= READ_STATUS_REG2_CMD;
			break;
		case 3 :
			s_command.Instruction 	= READ_STATUS_REG3_CMD;
			break;
		default :
			s_command.Instruction 	= READ_STATUS_REG1_CMD;
			break;
	}
	s_command.InstructionMode 	= QSPI_INSTRUCTION_1_LINE;
	s_command.AddressMode 			= QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode 					= QSPI_DATA_1_LINE;
	s_command.DummyCycles 			= 0;
	s_command.NbData 						= 1;
	s_command.DdrMode 					= QSPI_DDR_MODE_DISABLE;
	s_command.SIOOMode 					= QSPI_SIOO_INST_EVERY_CMD;

  /* Configure the command */
  if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Set S# timing for Read command */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_3_CYCLE);

  /* Reception of the data */
  if (HAL_QSPI_Receive(&hqspi, pStatus, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Restore S# timing for nonRead commands */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_6_CYCLE);

  return HAL_OK;
}

/**
  * @brief This function is used to read manufacturer and device ID.
	*					It uses Read Manufacturer / Device ID command to read it.
	* @param pManufacturerDeviceID pointer to Manufacturer Manufacturer ID (EFh for Winbond) and DeviceID Device ID (18h for W25Q256JW).
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_ReadManufacturerDeviceID(uint8_t* pManufacturerDeviceID)
{
  QSPI_CommandTypeDef s_command;
	/* Initialize the read status command */
	s_command.Instruction 			= READ_MANU_DEV_ID_CMD;
	s_command.InstructionMode 	= QSPI_INSTRUCTION_1_LINE;
	s_command.AddressMode 			= QSPI_ADDRESS_1_LINE;
	s_command.AddressSize       = QSPI_ADDRESS_24_BITS;
  s_command.Address           = 0;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode 					= QSPI_DATA_1_LINE;
	s_command.DummyCycles 			= 0;
	s_command.NbData 						= 2;
	s_command.DdrMode 					= QSPI_DDR_MODE_DISABLE;
	s_command.SIOOMode 					= QSPI_SIOO_INST_EVERY_CMD;

  /* Configure the command */
  if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Set S# timing for Read command */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_3_CYCLE);

  /* Reception of the data */
  if (HAL_QSPI_Receive(&hqspi, pManufacturerDeviceID, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Restore S# timing for nonRead commands */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_6_CYCLE);
  return HAL_OK;
}

/**
  * @brief This function is used to read Unique ID.
	*					It uses Read Unique ID Number command to read it.
	* @param pUniqueID pointer to a factory-set read-only 64-bit Unique ID number.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_ReadUniqueID(uint8_t* pUniqueID)
{
  QSPI_CommandTypeDef s_command;
	/* Initialize the read status command */
	s_command.Instruction 			= READ_UNIQUE_ID_CMD;
	s_command.InstructionMode 	= QSPI_INSTRUCTION_1_LINE;
	s_command.AddressMode 			= QSPI_ADDRESS_1_LINE;
	s_command.AddressSize       = QSPI_ADDRESS_24_BITS;
  s_command.Address           = 0;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode 					= QSPI_DATA_1_LINE;
	s_command.DummyCycles 			= 32;
	s_command.NbData 						= 8;
	s_command.DdrMode 					= QSPI_DDR_MODE_DISABLE;
	s_command.SIOOMode 					= QSPI_SIOO_INST_EVERY_CMD;

  /* Configure the command */
  if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Set S# timing for Read command */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_3_CYCLE);

  /* Reception of the data */
  if (HAL_QSPI_Receive(&hqspi, pUniqueID, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Restore S# timing for nonRead commands */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_6_CYCLE);
  return HAL_OK;
}

/**
  * @brief This function is used to read JEDEC ID.
	*					It uses Read JEDEC ID command to read it.
	* @param pJEDECID pointer to JEDEC ID.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_ReadJEDECID(uint8_t* pJEDECID)
{
  QSPI_CommandTypeDef s_command;
	/* Initialize the read status command */
	s_command.Instruction 			= READ_JEDEC_ID_CMD;
	s_command.InstructionMode 	= QSPI_INSTRUCTION_1_LINE;
	s_command.AddressMode 			= QSPI_ADDRESS_NONE;
	s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	s_command.DataMode 					= QSPI_DATA_1_LINE;
	s_command.DummyCycles 			= 0;
	s_command.NbData 						= 3;
	s_command.DdrMode 					= QSPI_DDR_MODE_DISABLE;
	s_command.SIOOMode 					= QSPI_SIOO_INST_EVERY_CMD;

  /* Configure the command */
  if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Set S# timing for Read command */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_3_CYCLE);

  /* Reception of the data */
  if (HAL_QSPI_Receive(&hqspi, pJEDECID, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Restore S# timing for nonRead commands */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_6_CYCLE);
  return HAL_OK;
}

/**
  * @brief This function is used to erase security register.
	*					It uses Erase Security Registers command to do it.
	* @param RegisterNum Security register number form 1 to 3.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_Erase_Security_Reg(uint8_t RegisterNum)
{
  QSPI_CommandTypeDef s_command;

  /* Initialize the erase command */
	switch(RegisterNum){
		case 1 :
			s_command.Address           = SECURITY_REGISTER1_ADD;
			break;
		case 2 :
			s_command.Address           = SECURITY_REGISTER2_ADD;
			break;
		case 3 :
			s_command.Address           = SECURITY_REGISTER3_ADD;
			break;
		default :
			s_command.Address           = 0;
			break;
	}
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = ERASE_SECURITY_REGISTER_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_1_LINE;
  s_command.AddressSize       = QSPI_ADDRESS_24_BITS;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_NONE;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Enable write operations */
  if (QSPI_WriteEnable() != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Send the command */
  if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Configure automatic polling mode to wait for end of erase */
  if (QSPI_AutoPollingMemReady() != HAL_OK)
  {
    return HAL_ERROR;
  }

  return HAL_OK;
}

/**
  * @brief This function is used to write a data to the security register.
	*					It uses Program Security Registers command to do it.
	* @param RegisterNum Security register number form 1 to 3.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_Write_Security_Reg(uint8_t* pData, uint8_t RegisterNum, uint8_t WriteAddr, uint8_t Size)
{
  QSPI_CommandTypeDef s_command;
	uint32_t current_size;
	
  /* Calculation of the size between the write address and the end of the register */
  current_size = W25Q256JW_SECURITY_REGISTER_SIZE - WriteAddr;

  /* Check if the size of the data is less than the remaining place in the register */
  if (current_size > Size)
  {
    current_size = Size;
  }
	else
	{
		return HAL_ERROR;
	}

	
  /* Initialize the write command */
	switch(RegisterNum){
		case 1 :
			s_command.Address           = SECURITY_REGISTER1_ADD + WriteAddr;
			break;
		case 2 :
			s_command.Address           = SECURITY_REGISTER2_ADD + WriteAddr;
			break;
		case 3 :
			s_command.Address           = SECURITY_REGISTER3_ADD + WriteAddr;
			break;
		default :
			s_command.Address           = 0;
			break;
	}
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = PROG_SECURITY_REGISTER_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_1_LINE;
  s_command.AddressSize       = QSPI_ADDRESS_24_BITS;
	s_command.NbData  					= current_size;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_1_LINE;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	/* Enable write operations */
	if (QSPI_WriteEnable() != HAL_OK)
	{
		return HAL_ERROR;
	}

	/* Configure the command */
	if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	{
		return HAL_ERROR;
	}

	/* Transmission of the data */
	if (HAL_QSPI_Transmit(&hqspi, pData, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	{
		return HAL_ERROR;
	}

	/* Configure automatic polling mode to wait for end of program */
	if (QSPI_AutoPollingMemReady() != HAL_OK)
	{
		return HAL_ERROR;
	}

  return HAL_OK;
}


/**
  * @brief This function is used to read the security register.
	*					It uses Fast Read Quad Output command to read data.
	* @param pData Pointer to read buffer data.
	* @param ReadAddr Address to be read.
	* @param Size Number of bytes to be read.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_Read_Security_Reg(uint8_t* pData,  uint8_t RegisterNum, uint8_t ReadAddr, uint8_t Size)
{
  QSPI_CommandTypeDef s_command;
	uint32_t current_size;
	
  /* Calculation of the size between the read address and the end of the register */
  current_size = W25Q256JW_SECURITY_REGISTER_SIZE - ReadAddr;
	
	/* Check if the size of the data is less than the remaining place in the register */
	if (current_size > Size)
  {
    current_size = Size;
  }
	else
	{
		return HAL_ERROR;
	}
	/* Initialize the read command */
	switch(RegisterNum){
		case 1 :
			s_command.Address       = SECURITY_REGISTER1_ADD + ReadAddr;
			break;
		case 2 :
			s_command.Address       = SECURITY_REGISTER2_ADD + ReadAddr;
			break;
		case 3 :
			s_command.Address       = SECURITY_REGISTER3_ADD + ReadAddr;
			break;
		default :
			s_command.Address       = 0;
			break;
	}
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = READ_SECURITY_REGISTER_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_1_LINE;
  s_command.AddressSize       = QSPI_ADDRESS_24_BITS;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_1_LINE;
  s_command.DummyCycles       = W25Q256JW_DUMMY_CYCLES_READ;
  s_command.NbData            = current_size;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Configure the command */
  if (HAL_QSPI_Command(&hqspi, &s_command, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Set S# timing for Read command */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_3_CYCLE);

  /* Reception of the data */
  if (HAL_QSPI_Receive(&hqspi, pData, HAL_QSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  /* Restore S# timing for nonRead commands */
  MODIFY_REG(hqspi.Instance->DCR, QUADSPI_DCR_CSHT, QSPI_CS_HIGH_TIME_6_CYCLE);

  return HAL_OK;
}

/**
  * @brief This function is used to write data on sector.
	*					It uses CSP_QSPI_Write function 16 times.
	* @param pData Pointer to write buffer data.
	* @param Sector_num Number of sector to be write.
  * @note		pData MUST be pointer to data with one sector size.
  * @retval HAL status
  */
HAL_StatusTypeDef CSP_QSPI_Write_Sector(uint8_t* pData, uint32_t Sector_num)
{
	for(uint8_t page_num = 0; page_num < FLASH_MEMORY_PAGE_IN_SECTOR_NUM; page_num++)
	{
		CSP_QSPI_Write_4Byte_Add(pData + (page_num * FLASH_MEMORY_PAGE_SIZE), (Sector_num*FLASH_MEMORY_SECTOR_SIZE)+(page_num*FLASH_MEMORY_PAGE_SIZE), FLASH_MEMORY_PAGE_SIZE);
	}
  return HAL_OK;
}

/**
 * @brief  Save ECG data on Flash Memory
 *
 */
void Save_ECG_data(void *argument){
	UNUSED(argument);
	int32_t ECGSaveRawData32;
	for(;;)
	{
		osThreadFlagsWait( 1, osFlagsWaitAny, osWaitForever);

		/* Write a Sector of ECG Data */
		memcpy(SectorToWriteData, compression_output, FLASH_MEMORY_SECTOR_SIZE);
		CSP_QSPI_Write_Sector(SectorToWriteData, ECG_data_Sector_Num);
		ECG_data_Sector_Num++;
//		osDelay(80);
		memcpy(SectorToWriteData, &compression_output[FLASH_MEMORY_SECTOR_SIZE], FLASH_MEMORY_SECTOR_SIZE);
		CSP_QSPI_Write_Sector(SectorToWriteData, ECG_data_Sector_Num);
		ECG_data_Sector_Num++;
	}
}

void Save_ECG_callback(void)
{
	osThreadFlagsSet( MyTaskSaveECGId, 1 );
}